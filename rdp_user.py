from pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval


__all__ = ['ContactMechanismUser', 'ContactMechanism_ContactMechanismUser']

__metaclass__ = PoolMeta


class ContactMechanismUser(ModelSQL, ModelView):
    """Users & Passwords"""
    __name__ = 'party.contact_mechanism_users'

    name = fields.Char(string='RDP user', required=True)
    pwd = fields.Char(string='Password', required=True)
    group = fields.Many2One('res.group', 'Group')
    party = fields.Many2One('party.party', 'Party')
    
    
class ContactMechanism_ContactMechanismUser(ModelSQL, ModelView):
    """Model between Contact Mechanism & Contact Mechanism Users"""
    __name__ = 'party.contact_mechanism-contact_mechanism_users'

    contact_mechanism = fields.Many2One('party.contact_mechanism', 'Contact Mechanism', ondelete='CASCADE',)
    rdp_user = fields.Many2One('party.contact_mechanism_users', 'RDP User', ondelete='CASCADE', select=True,
                               domain=[('party', '=', Eval('_parent_contact_mechanism', {}).get('party'))],
                               depends=['_parent_contact_mechanism.party'], )
    sequence = fields.Integer('Sequence')

    @classmethod
    def __setup__(cls):
        super(ContactMechanism_ContactMechanismUser, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))
        cls._buttons.update({'connect_rdp': {'invisible': Eval('type') == 'rdp', }, })
        cls._sql_constraints = [
            ('rdpusr-cm_uniq', 'UNIQUE(rdp_user, contact_mechanism)',
             'Combination of User and Contact Mechanism must be unique.')
        ]

    @ModelView.button
    def connect_rdp(self):
        self.contact_mechanism.connect_rdp(self.rdp_user)
