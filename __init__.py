from trytond.pool import Pool
from party import ContactMechanism
from .rdp_user import ContactMechanismUser, ContactMechanism_ContactMechanismUser


def register():
    Pool.register(
        ContactMechanism,
        ContactMechanismUser,
        ContactMechanism_ContactMechanismUser,
        module='party_contact_mechanism_rdp', type_='model')