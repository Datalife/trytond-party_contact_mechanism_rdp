import subprocess
from trytond.pyson import Eval, Equal
from trytond.pool import PoolMeta
from trytond.model import fields, ModelView


__all__ = ['ContactMechanism']

__metaclass__ = PoolMeta


class ContactMechanism:
    __name__ = 'party.contact_mechanism'

    rdp = fields.Function(fields.Char('RDP',
                                      states={'invisible': Eval('type') != 'rdp',
                                              'required': Eval('type') == 'rdp',
                                              'readonly': ~Eval('active', True), },
                                      depends=['value', 'type', 'active']), 'get_value', setter='set_value')

    rdp_user = fields.Function(fields.Many2One('party.contact_mechanism_users', string='RDP user',
                                               select=True, depends=['value', 'type', 'active'],
                                               states={'invisible': Eval('type') != 'rdp',
                                                       'required': Eval('type') == 'rdp',
                                                       'readonly': ~Eval('active', True), },
                                               ), 'get_default_user')

    available_users = fields.One2Many('party.contact_mechanism-contact_mechanism_users', 'contact_mechanism',
                                      'Available users', depends=['value', 'type', 'active'],
                                      states={'invisible': Eval('type') != 'rdp',
                                              },
                                      )

    def get_default_user(self, name):
        res = getattr(self, 'available_users', [])
        if not res:
            return None
        return res[0].rdp_user.id

    @classmethod
    def set_default_user(cls, users, field_name, user_id):
        pass

    @classmethod
    def __setup__(cls):
        super(ContactMechanism, cls).__setup__()
        cls.type.selection.append(('rdp', 'RDP'))
        cls.other_value._field.states['invisible'] |= Equal(Eval('type'), 'rdp')
        cls.url._field.states['invisible'] |= Equal(Eval('type'), 'rdp')
        cls._buttons.update({'connect_rdp': {'invisible': Eval('type') != 'rdp', }, })

    def _change_value(self, value):
        values = super(ContactMechanism, self)._change_value(value)
        values['rdp'] = value
        return values

    @ModelView.button
    def connect_rdp(self, user=None):
        try:
            if not user:
                user = self.rdp_user
            subprocess.call(
                'rdesktop {0} -u {1} -p {2} -k es -g 1280x768 -a 16 -z -x l -5 -r sound:off'.format(
                    self.rdp, user.name, user.pwd),  stderr=subprocess.STDOUT, shell=True)
        except:
            pass

    @fields.depends('rdp', 'type')
    def on_change_rdp(self):
        return self._change_value(self.rdp)